Math.randint = function(max, min=0) {
  min = this.ceil(min);
  max = this.floor(max);
  return this.floor(this.random() * (max - min)) + min;
}

Object.prototype.keys = function() {
  return Object.keys(this);
}

const canvas = document.getElementById('stage');
const ctx = canvas.getContext("2d");

const colors = {
  'red': 100,
  'blue': 30,
  'green': 30,
  'black': 0,
};

const bomb = new Audio('assets/bomb.mp3');
const boom = new Audio('assets/grenade.mp3');
const fire = new Audio('assets/fire.mp3');
const quack = new Audio('assets/quack.mp3');

const heart = new Image();
heart.src = "assets/heart.png";
heart.height = 26;
////////////////////////////////////////////////////////////////////
//  S P R I T E
////////////////////////////////////////////////////////////////////
class Sprite {
  constructor (src, x=0, y=0, w=0, h=0, vx=0, vy=0) {
    this.img = new Image();
    this.img.src = src;
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;
    this.vx = vx;
    this.vy = vy;
    this.alive = true;
    this.tolerance = 10;
  }
  update (time) {
    this.x += this.vx;
    this.y += this.vy;
  }
  draw (time) {
    ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
    /* * /
    ctx.beginPath();
    ctx.rect(this.x+this.tolerance, this.y+this.tolerance, this.width-2*this.tolerance, this.height-2*this.tolerance);
    ctx.stroke();
    /* */
  }
  collides (obj) {
    // se non separati in orizzontale e non separati in verticale
    return (
        !(obj.x + obj.width - obj.tolerance < this.x + this.tolerance
          || this.x + this.width - this.tolerance < obj.x + obj.tolerance)
      &&
        !(obj.y + obj.height - obj.tolerance < this.y + this.tolerance
          || this.y + this.height - this.tolerance < obj.y + obj.tolerance)
    );
  }
}
////////////////////////////////////////////////////////////////////
//  D U C K
////////////////////////////////////////////////////////////////////
class Duck extends Sprite {
  constructor (x, y) {
    super("assets/duck_0.png", x,y , 81, 93, -10, 0);
    this.sources = [
      "assets/duck_0.png",
      "assets/duck_1.png",
      "assets/duck_2.png",
      "assets/duck_3.png",
      "assets/duck_4.png",
      "assets/duck_5.png",
      "assets/duck_6.png",
      "assets/duck_7.png",

    ];
    this.currentImage = 0;
    this.deltaAnime = 100;  // ms ogni quanto cambiare img
    this.lastChange = 0;
    this.tolerance = 30;

  }
  update (time) {
    super.update(time);
    if (this.x + this.width <= 0) {
      this.alive = false;
    }


    if (time - this.lastChange > this.deltaAnime) {
      this.currentImage = (this.currentImage+1) % this.sources.length;
      this.lastChange = time;
      this.img.src = this.sources[this.currentImage];
    }
  }
  draw (time) {
    if (this.alive) {
      super.draw(time);
    }
  }

  collides (obj) {
    // se non separati in orizzontale e non separati in verticale
    return (
        !(obj.x + obj.width - obj.tolerance < this.x + this.tolerance
          || this.x + this.width - this.tolerance < obj.x + obj.tolerance)
      &&
        !(obj.y + obj.height - obj.tolerance < this.y + this.tolerance
          || this.y + this.height - this.tolerance < obj.y + obj.tolerance)
    );
  }
}


////////////////////////////////////////////////////////////////////
//  B U L L E T
////////////////////////////////////////////////////////////////////
class Bullet extends Sprite {
  constructor(x, y) {
    super('assets/calpis.png', x, y, 29, 27, 14, 0);
  }
  update(time) {
    super.update(time);
    if (this.x > 800) {
      this.alive = false;
    }
  }
}

////////////////////////////////////////////////////////////////////
//  P L A N E
////////////////////////////////////////////////////////////////////
class Plane extends Sprite {
  constructor () {
    super("assets/Fly_1.png", 100, 100, 147, 95, 5, 5);
    this.sources = [
      "assets/Fly_1.png",
      "assets/Fly_2.png"
    ];
    this.currentImage = 0;
    this.deltaAnime = 50;  // ms ogni quanto cambiare img
    this.lastChange = 0;

    this.speed = 10;
    canvas.addEventListener('mousemove', event => {
      this.mouse = {
        x: event.offsetX - this.width/2,
        y: event.offsetY - this.height/2
      }
    });
  }
  update (time) {
    if (this.mouse) {
      var a = Math.atan2(this.mouse.y-this.y, this.mouse.x-this.x);
      var r = Math.sqrt((this.mouse.y-this.y)**2+(this.mouse.x-this.x)**2);
      if (r <= this.speed) {
        this.x = this.mouse.x;
        this.y = this.mouse.y;
      } else {
        this.x += this.speed * Math.cos(a);
        this.y += this.speed * Math.sin(a);
      }
    }
    if (time - this.lastChange > this.deltaAnime) {
      this.currentImage = (this.currentImage+1) % this.sources.length;
      this.lastChange = time;
      this.img.src = this.sources[this.currentImage];
    }
  }

  draw (time) {
    super.draw(time);
  }
}

////////////////////////////////////////////////////////////////////
// S T A G E
////////////////////////////////////////////////////////////////////
class Stage extends Sprite {
  constructor (w=800, h=400) {
    super("assets/bg.png", 0, 0, w, h, -5, 0);
    this.balloons = new ListFactory();
    this.bullets = new ListFactory();
    this.plane = new Plane();
    this.duck = new Duck();
    this.score = 0;
    this.nbullets = 0;
    this.motor = new Audio('assets/airplane.mp3');
    this.motor.loop = true;
    this.motor.volume = .3;
    this.keydown = (event) => this.onKeydown(event);
    this.lives = 3;
  }
  onKeydown (event) {
    if (this.nbullets > 0) {
      this.nbullets--;
      this.bullets.push(new Bullet(this.plane.x + 92, this.plane.y + 61));
      let f = fire.cloneNode();
      f.volume = 0.2;
      f.play();
    }
  }
  start() {
    window.addEventListener('keydown', this.keydown);
    this.motor.play();
  }
  stop() {
    window.removeEventListener('keydown', this.keydown);
    this.motor.pause();
  }
  // aggiorna lo stato dell'oggetto
  update(time) {
    super.update(time);
    if (this.x <= -this.width) {
      this.x = 0;
    }
    this.balloons.visit(b => b.update(time));
    this.bullets.visit(b => b.update(time));
    this.plane.update(time);
    this.duck.update(time);
    // Collisioni aereo con palloncini
    this.balloons.map(
      bal => {
        if (this.plane.collides(bal)) {
          if (bal.color == 'black') {
            this.nbullets += 10;
          }
          bal.alive = false;
          //this.score -= colors[bal.color];
          this.lives--;

          bomb.cloneNode().play();
        }
        return bal;
      }
    )
    // Collisione duck
    this.bullets.map(
      bul => {
        if (bul.collides(this.duck)) {
          bul.alive = false;
          this.duck.alive = false;
          console.log(this.duck.alive);
          this.lives++;
          quack.cloneNode().play();

        }

        return bul;
      }

    );
    // Collisioni pallottole con palloncini
    this.bullets.map(
      bul => {
        this.balloons.map(bal => {
          if (bal.collides(bul)) {
            bul.alive = false;
            bal.alive = false;
            this.score += colors[bal.color];
            if (bal.color == 'black') {
              this.nbullets += 10;
            }
            boom.cloneNode().play();
          }
          return bal;
        });
        return bul;
      }
    )
    // amministrazione morti
    this.balloons = this.balloons.filter(x =>x.alive);
    this.bullets = this.bullets.filter(x => x.alive);


    // balloon spawning
    if (this.balloons.size() < 20) {
      if (Math.random() > 0.95) {
        let c = colors.keys()[Math.randint(4)];
        this.balloons.push(new Balloon(Math.random()*750+50, 400, c));
        //console.log("Balloon spawned");
      }
    }
    if (this.duck.alive === false && Math.random() > 0.995) {

        this.duck = new Duck(800, Math.random()*300+50);

    }
  }
  // disegna l'oggetto sul canvas
  draw(time) {
    super.draw(time);
    ctx.drawImage(this.img, this.x + this.width, 0);
    // disegno degli sprite
    this.plane.draw(time);
    this.duck.draw(time);
    this.balloons.visit(b => b.draw(time));
    this.bullets.visit(b => b.draw(time));
    // aggiornamenti interfaccia utente
    timeval.innerHTML =
      (time/1000).toFixed(2).toString().replace('.', ':');
    score.innerHTML = this.score;
    cval.innerHTML = this.nbullets;

    while (lives.children.length > this.lives) {
      lives.firstChild.remove();
    }
    while (lives.children.length < this.lives) {
      lives.appendChild(heart.cloneNode());
    }
  }
}

////////////////////////////////////////////////////////////////////
// B A L L O O N
////////////////////////////////////////////////////////////////////
class Balloon extends Sprite {
  constructor (x=0, y=0, color='red') {
    super(`assets/balloon_${color}.png`, x, y, 51, 64, -3, -3);
    this.color = color;
  }
  update(time) {
    super.update(time);
    this.y += (Math.random() - 0.5) * 3;
    this.x += (Math.random() - 0.5) * 6;
    // se fuori schermo
    if (this.y < -this.height || this.x < -this.width) {
      this.alive = false;
      //console.log("Balloon dead");
    }
  }
}

////////////////////////////////////////////////////////////////////
// G A M E
////////////////////////////////////////////////////////////////////
class Game {
  constructor() {
    this.currentTime = 0;const heart = new Image();
    this.startTime = null;
    this.end = false;
  }
  start() {
    this.end = false;
    this.stage = new Stage();
    document.getElementById('start').style.display = 'none';
    this.currentTime = 0;
    this.startTime = null;
    this.stage.score = 0;
    this.stage.nbullets = 12;
    this.stage.lives = 3;
    this.stage.start();
  }
  step(time) {
    if (!this.startTime) {
      this.startTime = time;
    }

    this.currentTime = time - this.startTime;
    this.stage.update(this.currentTime);
    this.stage.draw(this.currentTime);

  }
  stop() {
    document.getElementById('start').style.display = '';
    this.stage.stop();
  }
}

////////////////////////////////////////////////////////////////////
//  M A I N
////////////////////////////////////////////////////////////////////
const game = new Game();

function start(btn) {
  game.start();
  step();
}

function step() {
  requestAnimationFrame(run);
}

// animation loop
function run(time) {
  game.step(time);
  if (!game.end  && !game.stage.lives <= 0 && game.stage.score < 2000) {
    setTimeout(step, 0);
  } else {
    game.stop();
  }
}
